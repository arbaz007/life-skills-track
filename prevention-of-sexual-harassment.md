# Prevention of Sexual Harassment

1. **Unwanted Comments:**
   - Making inappropriate jokes or comments about someone's body or appearance.

2. **Unwanted Touching:**
   - Touching someone without their permission or in a way that makes them uncomfortable.

3. **Pressure for Sexual Favors:**
   - Trying to force or pressure someone into sexual activities against their will.

4. **Threats or Intimidation:**
   - Using threats or intimidation to make someone do something of a sexual nature.

5. **Creating an Unpleasant Environment:**
   - Making the workplace or environment uncomfortable due to comments, gestures, or actions related to someone's gender.

# What would you do in case you face or witness any incident or repeated incidents of such behaviour?

If you see or experience someone doing things that make you uncomfortable because of your gender, here's what you can do:

1. **Talk to someone you trust:**
   - Share what happened with a friend, family member, or coworker.

2. **Say it's not okay:**
   - If you feel safe, tell the person their actions are not okay and make you uncomfortable.

3. **Report it:**
   - Tell a manager, supervisor, or someone in charge at work. They can help address the situation.

4. **Keep a record:**
   - Write down what happened, when, and where. This can be helpful if you need to talk to someone about it later.

5. **Know your rights:**
   - Learn about the rules at your workplace or in your community. Knowing your rights can help you understand what actions can be taken.

